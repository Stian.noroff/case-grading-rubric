document.addEventListener("DOMContentLoaded", function() {
    populateScores([8, 8, 8, 8, 68], ['score1', 'score2', 'score3', 'score4', 'score5']);
});

function populateScores(maxScores, ids) {
    maxScores.forEach((maxScore, index) => {
        let select = document.getElementById(ids[index]);
        for (let i = 0; i <= maxScore; i++) {
            let option = document.createElement("option");
            option.text = i;
            option.value = i;
            select.appendChild(option);
        }

        select.addEventListener('change', () => {
            calculateGrade();
        });
    });
}

function calculateGrade() {
    const maxScores = [8, 8, 8, 8, 68];
    let totalScore = 0;

    for (let i = 0; i < 5; i++) {
        const scoreElement = document.getElementById(`score${i + 1}`);
        if (!scoreElement.value) {
            document.getElementById("result").innerHTML = "Please select all values";
            return;
        }
        const score = parseFloat(scoreElement.value);
        totalScore += score; 
        document.getElementById(`totalScore${i + 1}`).innerText = `${score.toFixed(2)}%`;  // Update the respective cell in the table
    }

    document.getElementById("result").innerHTML = `Grade: ${totalScore.toFixed(2)}%`;
}

function highlightScore(index, score, maxScore) {
    const rubricRows = document.querySelectorAll("#grading-rubric tbody tr");
    const rubricRow = rubricRows[index];
    const cells = rubricRow.querySelectorAll("td");

    for (let i = 1; i < cells.length; i++) {
        const cell = cells[i];
        if (cell.innerText.includes(score.toString())) {
            cell.classList.add("table-active");
            break;
        }
    }
}

document.getElementById('copyTable').addEventListener('click', function() {
    var gradingRubric = document.getElementById('grading-rubric');
    html2canvas(gradingRubric).then(function(canvas) {
        var context = canvas.getContext('2d');
        context.globalCompositeOperation = 'destination-over';
        context.fillStyle = '#FFFFFF';
        context.fillRect(0, 0, canvas.width, canvas.height);
        var img = canvas.toDataURL("image/png");
        var link = document.createElement('a');
        link.download = 'rubric.png';
        link.href = img;
        link.click();
    });
});